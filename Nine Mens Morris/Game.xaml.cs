﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Nine_Mens_Morris
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Game : Page
    {
        Button              play_again_button;
        Ellipse             recent_clicked_position, moving_dies, dies_to_be_remove;
        Ellipse[]           white_dies, black_dies, highlighted_positions;
        Ellipse[,]          positions;
        Rectangle[]         back_surface, highlighted_paths, trayo_Error;
        Rectangle[,]        trayo;
        Rectangle[, ,]      path;
        Storyboard          move_dies, remove_dies, winning_animation;
        Storyboard[]        blink_trayo, blink_trayo_Error;
        TextBlock           message;
        DoubleAnimation     da_hide;
        DoubleAnimation[]   da_Txy, da_blink, da_blink_Error; 
        SolidColorBrush     highlight_color, normal_color, trayo_color, trayo_color_Error, position_color, p1_dies_color, p2_dies_color;
        SolidColorBrush[]   back_surface_color;
        Color[]             my_colors;
        bool[,]             trayo_status_of_dies;
        bool                player_in_flying_mode, white_dies_in_flying_mode, black_dies_in_flying_mode, dont_check_for_deadlock_more, initial_stage = true, two_white_dies_placed, Enable_click_on_position = true, Enable_click_on_dies, all_dies_placed, trayo_in_vertical_panel, trayo_in_horizontal_panel, game_over, sound_OFF, one_or_more_path_highlighted;
        int                 dies_Sno, pos_index_in_vertical_panel, pos_index_in_horizontal_panel, player_bit = 1, opponent_player_bit = 2, position_index = 0, No_of_white_dies = 9, No_of_black_dies = 9;
        int[, ,]            value;
        GameComponents      Gc;
        AnimationManager    Am;
        AudioManager        Sm;
        MediaElement        destination_selected, white_dies_placed_sound, black_dies_placed_sound, dies_is_in_mill_sound, highlight_path_sound, dies_is_blocked_sound;
        ComponentMaintainer cm;

        public Game()
        {
            this.InitializeComponent();
            cm = new ComponentMaintainer(70000);
            maintainComponent();
            create_sound_components();
        }

        void maintainComponent()
        {
            cm.maintainShape(background_1);
            cm.maintainShape(background_2);
            cm.maintainAppbar(Options);
            cm.maintainButton(home_button);
            cm.maintainButton(restart_button);
            cm.maintainButton(sound_button);
        }

        void create_sound_components()
        {
            Sm = new AudioManager(myGrid);
            white_dies_placed_sound = Sm.create_sound(2);
            black_dies_placed_sound = Sm.create_sound(3);
            dies_is_in_mill_sound = Sm.create_sound(4);
            highlight_path_sound = Sm.create_sound(5);
            dies_is_blocked_sound = Sm.create_sound(6);
            destination_selected = Sm.create_sound(7);
            destination_selected.Volume = 0.3;
            highlight_path_sound.Volume = 0.3;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            my_colors = e.Parameter as Color[];
            Gc = new GameComponents(my_colors, myGrid);
            Am = new AnimationManager();
            set_colors();
            GeneratComponents();
            create_move_dies();
            create_blink_trayo();
            create_remove_dies();
            set_message(1);
            show_message.Completed += show_message_Completed;
        }

        void set_colors()
        {
            Options.Background = new SolidColorBrush(my_colors[9]);
            current_player_color_1.Color =
            current_player_color_2.Value =
            current_player_color_3.Value = my_colors[0];
            opponent_player_color_1.Color =
            opponent_player_color_2.Color =
            opponent_player_color_3.Color =
            opponent_player_color_4.Value =
            opponent_player_color_5.Value = my_colors[1];
            back_surface_color = new SolidColorBrush[3];
            back_surface_color[0] = new SolidColorBrush(my_colors[2]);
            back_surface_color[1] = new SolidColorBrush(my_colors[3]);
            back_surface_color[2] = new SolidColorBrush(my_colors[4]);
            normal_color = new SolidColorBrush(my_colors[5]);
            highlight_color = new SolidColorBrush(my_colors[6]);
            trayo_color = new SolidColorBrush(my_colors[7]);
            trayo_color_Error = new SolidColorBrush(my_colors[8]);
            position_color = new SolidColorBrush(my_colors[9]);
            p1_dies_color = new SolidColorBrush(my_colors[10]);
            p2_dies_color = new SolidColorBrush(my_colors[11]);
        }
        void GeneratComponents()
        {
            trayo_Error = new Rectangle[2];
            highlighted_paths = new Rectangle[4];
            highlighted_positions = new Ellipse[4];

            for (int i = 0; i < 4; i++)
            {
                if(i < 2)
                    trayo_Error[i] = new Rectangle();
                highlighted_paths[i] = new Rectangle();
                highlighted_positions[i] = new Ellipse();
            }
            value = new int[2, 24, 3];
            trayo_status_of_dies = new bool[2, 9];
            back_surface = new Rectangle[3];

            for (int i = 0; i < 3; i++)
            {
                back_surface[i] = Gc.get_back_surface(i);
                cm.maintainShape(back_surface[i]);
            }
            message = new TextBlock();
            message.Width = 500;
            message.FontSize = 40;
            message.TextWrapping = TextWrapping.Wrap;
            message.TextAlignment = TextAlignment.Center;
            message.FontFamily = new FontFamily("Tempus Sans ITC");
            message.Opacity = 0;
            myGrid.Children.Add(message);
            Storyboard.SetTarget(msg_Opacity, message);

            play_again_button = new Button();
            play_again_button.Content = "PLAY AGAIN";
            play_again_button.FontSize = 50;
            play_again_button.Height = 80;
            play_again_button.Width = 400;
            play_again_button.Tag = 2;
            play_again_button.BorderThickness = new Thickness(0, 0, 0, 0);
            play_again_button.Margin = new Thickness(483, 0, 0, 0);
            play_again_button.FontFamily = new FontFamily("Tempus Sans ITC");
            play_again_button.Click += option_clicked;
            play_again_button.Visibility = Visibility.Collapsed;
            cm.maintainButton(play_again_button);
            myGrid.Children.Add(play_again_button);

            path = new Rectangle[2, 8, 2];
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 8; j++)
                    for (int k = 0; k < 2; k++)
                    {
                        path[i, j, k] = Gc.get_path(i, j, k);
                        cm.maintainShape(path[i, j, k]);
                    }
            trayo = new Rectangle[2, 8];
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 8; j++)
                {
                    trayo[i, j] = Gc.get_trayo(i, j);
                    cm.maintainShape(trayo[i, j]);
                }
            positions = new Ellipse[8, 3];
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    positions[i, j] = Gc.get_position(i, j);
                    cm.maintainShape(positions[i, j]);
                    positions[i, j].PointerPressed += PointerPressed_on_position;
                }
            }
            white_dies = new Ellipse[9];
            black_dies = new Ellipse[9];
            for (int i = 0; i < 9; i++)
            {
                white_dies[i] = Gc.get_dies(i, 0);
                black_dies[i] = Gc.get_dies(i, 1);
                cm.maintainShape(white_dies[i]);
                cm.maintainShape(black_dies[i]);
                white_dies[i].PointerPressed += PointerPressed_on_dies;
                black_dies[i].PointerPressed += PointerPressed_on_dies;
            }
        }

        void create_move_dies()
        {
            move_dies = new Storyboard();
            da_Txy = Am.get_da_Txy(move_dies);
            move_dies.Completed += move_dies_Completed;
        }
        void move_dies_Completed(object sender, object e)
        {
            move_dies.Stop();
            moving_dies.Margin = recent_clicked_position.Margin;
            recent_clicked_position.Stroke = normal_color;

            if (initial_stage)
            {
                switch (player_bit)
                {
                    case 1: Sm.play_sound(white_dies_placed_sound); break;
                    case 2: Sm.play_sound(black_dies_placed_sound); break;
                }
                if (dies_Sno == 0) dies_Sno++;
                else { dies_Sno--; change_player_bit(); change_grid_color(); }
                click_setting_manager(1);
            }
            else
            {
                switch (opponent_player_bit)
                {
                    case 1: Sm.play_sound(white_dies_placed_sound); break;
                    case 2: Sm.play_sound(black_dies_placed_sound); break;
                }
                if (!trayo_in_vertical_panel && !trayo_in_horizontal_panel)
                {
                    if (game_over) winning_animation.Begin();
                    else click_setting_manager(4);
                }
                else
                {
                    if (trayo_in_vertical_panel) set_trayo_to_blink(0, 0);
                    if (trayo_in_horizontal_panel) set_trayo_to_blink(1, 0);
                }
            }
        }

        void create_remove_dies()
        {
            remove_dies = new Storyboard();
            da_hide = Am.get_da_hide(remove_dies);
            remove_dies.Completed += remove_dies_Completed;
        }
        void remove_dies_Completed(object sender, object e)
        {
            remove_dies.Stop();
            dies_to_be_remove.Visibility = Visibility.Collapsed;

            if (game_over)
                winning_animation.Begin();
            else
                click_setting_manager(4);
        }

        void create_blink_trayo()
        {
            blink_trayo = new Storyboard[2];
            blink_trayo_Error = new Storyboard[2];
            da_blink = new DoubleAnimation[2];
            da_blink_Error = new DoubleAnimation[2];
            for (int i = 0; i < 2; i++)
            {
                blink_trayo[i] = new Storyboard();
                blink_trayo_Error[i] = new Storyboard();
                da_blink[i] = Am.get_da_show(blink_trayo[i]);
                da_blink[i].RepeatBehavior = RepeatBehavior.Forever;
                da_blink_Error[i] = Am.get_da_show(blink_trayo_Error[i]);
                da_blink[i].AutoReverse = da_blink_Error[i].AutoReverse = true;
                blink_trayo_Error[i].Completed += blink_trayo_Error_completed;
            }
        }
        void blink_trayo_Error_completed(object sender, object e)
        {
            ((Storyboard)sender).Stop();
            if(sender.Equals(blink_trayo_Error[0]))
                trayo_Error[0].Fill = trayo_color;
            else
                trayo_Error[1].Fill = trayo_color;
        }

        void set_message(int Msg_no)
        {
            if (Msg_no < 4) message.Height = 150;
            else message.Height = 100;

            switch (Msg_no)
            {
                case 1: message.Text = "PHASE 1:" + '\n' + "place your pieces turn by turn on EMPTY positions"; break;
                case 2: message.Text = "Ha..Ha..Ha.." + '\n' + "You have to remove piece of opponent not your own"; break;
                case 3: message.Text = "PHASE 2:" + '\n' + "Now select your piece to move it to an adjacent place"; break;
                case 4: message.Text = "This piece is in MILL" + '\n' + "You can't remove it."; break;
                case 5: message.Text = "You are in FLYING mode now onwards"; break;
                case 6: message.Text = "This piece is NOT yet placed on board"; break;
                case 7: message.Text = "All pieces of opponent is in the Mills"; break;
                case 8: message.Text = "Not even single piece is able to MOVE"; break;
                case 9: message.Text = "No suffient pieces are remains to form a MILL"; break;
                case 10: message.Text = "You can place your piece on EMPTY position only"; break;
            }

            show_message.Begin();
        }
        void show_message_Completed(object sender, object e)
        {
            show_message.Stop();
            if (game_over)
                play_again_button.Visibility = Visibility.Visible;
        }

        void PointerPressed_on_position(object sender, PointerRoutedEventArgs e)
        {
            // position pressed on position 
            // 1. pressed by first player to place white_dies
            // 2. pressed by second player to place black dies
            // 3. when all dies placed to move dies
            if(show_message.GetCurrentTime() < TimeSpan.FromSeconds(5))
                show_message.Seek(TimeSpan.FromSeconds(5));
            if (Enable_click_on_position)
            {
                recent_clicked_position = (Ellipse)sender;
                if (!all_dies_placed)
                {
                    pos_index_in_vertical_panel = int.Parse(recent_clicked_position.Tag.ToString());
                    pos_index_in_horizontal_panel = get_conresponding_value_in_horizontal_panel(pos_index_in_vertical_panel);
                    value[0, pos_index_in_vertical_panel, player_bit] = value[1, pos_index_in_horizontal_panel, player_bit] = dies_Sno + 1;
                    if (initial_stage)
                    {
                        if (!two_white_dies_placed)
                        {
                            moving_dies = white_dies[dies_Sno];
                            if (dies_Sno == 1) two_white_dies_placed = true;
                        }
                        else
                        {
                            moving_dies = black_dies[dies_Sno];
                            if (dies_Sno == 1) { initial_stage = false; dies_Sno++; }
                        }
                    }
                    else
                    {
                        switch (player_bit)
                        {
                            case 1: moving_dies = white_dies[dies_Sno]; break;
                            case 2: moving_dies = black_dies[dies_Sno]; dies_Sno++; break;
                        }
                    }
                    recent_clicked_position.Stroke = highlight_color;
                    move_moving_dies();
                }
                else if (recent_clicked_position.Stroke == highlight_color)
                {
                    //before getting new position of recent_clicked_position 
                    //clear the values of dies being selected for movement
                    value[0, pos_index_in_vertical_panel, 0] = value[1, pos_index_in_horizontal_panel, 0] = 0;
                    int temp_dies_NO = value[0, pos_index_in_vertical_panel, player_bit];
                    trayo_status_of_dies[player_bit - 1, value[0, pos_index_in_vertical_panel, player_bit] - 1] = false;
                    value[0, pos_index_in_vertical_panel, player_bit] = value[1, pos_index_in_horizontal_panel, player_bit] = 0;

                    reset_trayo_status();

                    pos_index_in_vertical_panel = int.Parse(recent_clicked_position.Tag.ToString());
                    pos_index_in_horizontal_panel = get_conresponding_value_in_horizontal_panel(pos_index_in_vertical_panel);
                    value[0, pos_index_in_vertical_panel, player_bit] = value[1, pos_index_in_horizontal_panel, player_bit] = temp_dies_NO;

                    move_moving_dies();
                }
            }
        }
        void PointerPressed_on_dies(object sender, PointerRoutedEventArgs e)
        {
            if (show_message.GetCurrentTime() < TimeSpan.FromSeconds(5))
                show_message.Seek(TimeSpan.FromSeconds(5));
            if (Enable_click_on_dies)
            {
                if (all_dies_placed)
                {
                    pos_index_in_vertical_panel = int.Parse(((Ellipse)sender).Tag.ToString());
                    pos_index_in_horizontal_panel = get_conresponding_value_in_horizontal_panel(pos_index_in_vertical_panel);
                    if (!trayo_in_horizontal_panel && !trayo_in_vertical_panel)
                    {
                        if (value[0, pos_index_in_vertical_panel, 0] == player_bit)
                        {
                            moving_dies = (Ellipse)sender;
                            switch (player_bit)
                            {
                                case 1: player_in_flying_mode = white_dies_in_flying_mode; break;
                                case 2: player_in_flying_mode = black_dies_in_flying_mode; break;
                            }
                            if (player_in_flying_mode)
                                highlight_possible_destination(1);
                            else
                            {
                                make_normal_highlighted_destination(0);
                                highlight_possible_destination(0);
                            }
                        }
                        else
                            make_normal_highlighted_destination(0);
                    }
                    else proceed_to_remove((Ellipse)sender);
                }
                else if (((Ellipse)sender).Tag != null)
                {
                    pos_index_in_vertical_panel = int.Parse(((Ellipse)sender).Tag.ToString());
                    pos_index_in_horizontal_panel = get_conresponding_value_in_horizontal_panel(pos_index_in_vertical_panel);
                    proceed_to_remove((Ellipse)sender);
                }
                else
                    set_message(6);
            }
            else
                set_message(10);
        }

        void move_moving_dies()
        {
            click_setting_manager(0);
            moving_dies.Tag = pos_index_in_vertical_panel.ToString();
            value[0, pos_index_in_vertical_panel, 0] = value[1, pos_index_in_horizontal_panel, 0] = player_bit;
            da_Txy[0].To = (recent_clicked_position.Margin.Left - moving_dies.Margin.Left) / 2;
            da_Txy[1].To = (recent_clicked_position.Margin.Top - moving_dies.Margin.Top) / 2;
            Storyboard.SetTarget(da_Txy[0], moving_dies);
            Storyboard.SetTarget(da_Txy[1], moving_dies);
            trayo_in_vertical_panel = check_for_trayo_in(0);
            trayo_in_horizontal_panel = check_for_trayo_in(1);
            move_dies.Begin();
            Sm.play_sound(destination_selected);
            if (!initial_stage)
            {
                change_player_bit();
                if (!trayo_in_vertical_panel && !trayo_in_horizontal_panel)
                {
                    game_over_status();
                    change_grid_color();
                }
                if (all_dies_placed)
                {
                    switch (opponent_player_bit)
                    {
                        case 1:
                            if (!white_dies_in_flying_mode) make_normal_highlighted_destination(0);
                            else make_normal_highlighted_destination(1);
                            break;
                        case 2:
                            if (!black_dies_in_flying_mode) make_normal_highlighted_destination(0);
                            else make_normal_highlighted_destination(1);
                            break;
                    }
                }
            }
        }
        void proceed_to_remove(Ellipse ellipse)
        {
            dies_to_be_remove = ellipse;
            if (value[0, pos_index_in_vertical_panel, 0] == player_bit)
            {
                bool Ht = check_for_trayo_in(0);
                bool Vt = check_for_trayo_in(1);
                if (!Ht && !Vt)
                {
                    click_setting_manager(0);
                    Storyboard.SetTarget(da_hide, dies_to_be_remove);
                    dies_to_be_remove.Tag = "99";
                    value[0, pos_index_in_vertical_panel, 0] = value[1, pos_index_in_horizontal_panel, 0] = 0;
                    value[0, pos_index_in_vertical_panel, player_bit] = value[1, pos_index_in_horizontal_panel, player_bit] = 0;
                    remove_dies.Begin();

                    switch (player_bit)
                    {
                        case 1:
                            No_of_white_dies--;
                            switch (No_of_white_dies)
                            {
                                case 2: game_over = true; set_message(9); break;
                                case 3: set_message(5); white_dies_in_flying_mode = dont_check_for_deadlock_more = true; break;
                            }
                            break;
                        case 2:
                            No_of_black_dies--;
                            switch (No_of_black_dies)
                            {
                                case 2: game_over = true; set_message(9); break;
                                case 3: set_message(5); black_dies_in_flying_mode = dont_check_for_deadlock_more = true; break;
                            }
                            break;
                    }

                    if (trayo_in_vertical_panel) blink_trayo[0].Stop();
                    if (trayo_in_horizontal_panel) blink_trayo[1].Stop();
                    trayo_in_vertical_panel = trayo_in_horizontal_panel = false;
                    game_over_status();
                    change_grid_color();
                }
                else
                {
                    if (Ht) set_trayo_to_blink(0, 1);
                    if (Vt) set_trayo_to_blink(1, 1);
                    set_message(4);
                }
            }
            else
                set_message(2);
        }

        void game_over_status()
        {
            if (!game_over && !dont_check_for_deadlock_more)
            {
                switch (player_bit)
                {
                    case 1: check_for_game_completion_state(white_dies); break;
                    case 2: check_for_game_completion_state(black_dies); break;
                }
            }
            if (game_over)
            {
                switch (opponent_player_bit)
                {
                    case 1: create_winning_animation(white_dies, black_dies, No_of_white_dies, No_of_black_dies); break;
                    case 2: create_winning_animation(black_dies, white_dies, No_of_black_dies, No_of_white_dies); break;
                }
            }
        }
        void create_winning_animation(Ellipse[] winner_dies, Ellipse[] looser_dies, int no_of_winner_player_dies, int no_of_looser_player_dies)
        {
            switch (opponent_player_bit)
            {
                case 1:
                    opponent_player_color_4.Value = my_colors[10];
                    current_player_color_3.Value = my_colors[10];
                    break;
                case 2:
                    opponent_player_color_5.Value = my_colors[11];
                    current_player_color_2.Value = my_colors[11];
                    break;
            }
            // winner is opponent player bit
            winning_animation = new Storyboard();

            DoubleAnimation[] da_winner, da_looser;
            da_winner = new DoubleAnimation[no_of_winner_player_dies];
            da_looser = new DoubleAnimation[no_of_looser_player_dies];

            dies_Sno = 0;
            for (int i = 0; i < no_of_winner_player_dies; i++)
            {
                while (int.Parse(winner_dies[dies_Sno].Tag.ToString()) == 99) dies_Sno++;
                da_winner[i] = Am.get_da_Sx(winning_animation, 1);
                da_winner[i].AutoReverse = true;
                da_winner[i].RepeatBehavior = RepeatBehavior.Forever;
                Storyboard.SetTarget(da_winner[i], winner_dies[dies_Sno]);
                dies_Sno++;
            }
            dies_Sno = 0;
            for (int i = 0; i < no_of_looser_player_dies; i++)
            {
                while (int.Parse(looser_dies[dies_Sno].Tag.ToString()) == 99) dies_Sno++;
                da_looser[i] = Am.get_da_Sy(winning_animation, 2);
                Storyboard.SetTarget(da_looser[i], looser_dies[dies_Sno]);
                dies_Sno++;
            }
        }
        void check_for_game_completion_state(Ellipse[] dies)
        {
            if (all_dies_placed)
            {
                game_over = true;
                for (int i = 0; i < 9; i++)
                {
                    position_index = int.Parse(dies[i].Tag.ToString());
                    if (position_index == 99) continue;
                    game_over = game_over && !neighbour_position_is_empty();
                    if (!game_over) break;
                }
                if (game_over)
                    set_message(8);
            }
        }
        bool neighbour_position_is_empty()
        {
            switch (position_index % 3)
            {
                case 0: if (value[0, position_index + 1, 0] == 0) return true; break;
                case 1: if (value[0, position_index - 1, 0] == 0 || value[0, position_index + 1, 0] == 0) return true; break;
                case 2: if (value[0, position_index - 1, 0] == 0) return true; break;
            }
            position_index = get_conresponding_value_in_horizontal_panel(position_index);
            switch (position_index % 3)
            {
                case 0: if (value[1, position_index + 1, 0] == 0) return true; break;
                case 1: if (value[1, position_index - 1, 0] == 0 || value[1, position_index + 1, 0] == 0) return true; break;
                case 2: if (value[1, position_index - 1, 0] == 0) return true; break;
            }
            return false;
        }

        void set_trayo_to_blink(int target_panel, int purpose)
        {
            switch(target_panel)
            {
                case 0: position_index = pos_index_in_vertical_panel / 3; break;
                case 1: position_index = pos_index_in_horizontal_panel / 3; break;
            }
            switch(purpose)
            {
                case 0:
                    for (int i = 0; i < 3; i++)
                        trayo_status_of_dies[opponent_player_bit - 1, value[target_panel, 3 * position_index + i, opponent_player_bit] - 1] = true;
                    if (all_dies_of_opponent_is_in_trayo())
                    {
                        set_message(7);
                        change_grid_color();
                        trayo_in_horizontal_panel = trayo_in_vertical_panel = false;
                        trayo_Error[target_panel] = trayo[target_panel, position_index];
                        blink_trayo_Error[target_panel].Stop();
                        Storyboard.SetTarget(da_blink_Error[target_panel], trayo_Error[target_panel]);
                        blink_trayo_Error[target_panel].Begin();
                        click_setting_manager(4); 
                    }
                    else
                    {
                        Storyboard.SetTarget(da_blink[target_panel], trayo[target_panel, position_index]);
                        blink_trayo[target_panel].Begin();
                        click_setting_manager(2);
                    }
                    break;
                case 1:
                    trayo_Error[target_panel] = trayo[target_panel, position_index];
                    trayo_Error[target_panel].Fill = trayo_color_Error;
                    blink_trayo_Error[target_panel].Stop();
                    Storyboard.SetTarget(da_blink_Error[target_panel], trayo_Error[target_panel]);
                    blink_trayo_Error[target_panel].Begin();
                    break;
            }
        }
        bool all_dies_of_opponent_is_in_trayo()
        {
            bool result = true;
            for (int i = 0; i < dies_Sno; i++)
            {
                switch(player_bit)
                {
                    case 1: if (int.Parse(white_dies[i].Tag.ToString()) == 99) continue; break;
                    case 2: if (int.Parse(black_dies[i].Tag.ToString()) == 99) continue; break;
                }
                result = result && trayo_status_of_dies[player_bit - 1, i];
            }
            return result;
        }
        void reset_trayo_status()
        {
            int other_panel = 0;
            for (int target_panel = 0; target_panel < 2; target_panel++)
            {
                switch(target_panel)
                {
                    case 0: other_panel = 1; position_index = pos_index_in_vertical_panel / 3; break;
                    case 1: other_panel = 0; position_index = pos_index_in_horizontal_panel / 3; break;
                }
                position_index *= 3;
                for (int i = 0; i < 3; i++)
                {
                    switch(target_panel)
                    {
                        case 0: if (position_index + i == pos_index_in_vertical_panel) continue; break;
                        case 1: if (position_index + i == pos_index_in_horizontal_panel) continue; break;
                    }
                    if (value[target_panel, position_index + i, 0] == player_bit)
                        trayo_status_of_dies[player_bit - 1, value[target_panel, position_index + i, player_bit] - 1] = trayo_status_in_other_panel(other_panel, position_index + i);
                }
            }
        }
        bool trayo_status_in_other_panel(int target_panel, int position)
        {
            position /= 3;
            position *= 3;
            if ((player_bit == value[target_panel, position, 0]) && 
                (value[target_panel, position, 0] == value[target_panel, position + 1, 0]) && 
                (value[target_panel, position + 1, 0] == value[target_panel, position + 2, 0])) 
                return true;
            else
                return false;
        }

        void highlight_possible_destination(int mode)
        {
            switch(mode)
            {
                case 0:
                    one_or_more_path_highlighted = false;
                    switch(pos_index_in_vertical_panel % 3)
                    {
                        case 0:
                            if (value[0, pos_index_in_vertical_panel + 1, 0] == 0) 
                                highlight_destination(0, 0, 1, 0); break;
                        case 1:
                            if (value[0, pos_index_in_vertical_panel - 1, 0] == 0) 
                                highlight_destination(0, 0, 0, 0);
                            if (value[0, pos_index_in_vertical_panel + 1, 0] == 0) 
                                highlight_destination(1, 0, 2, 1); break;
                        case 2:
                            if (value[0, pos_index_in_vertical_panel - 1, 0] == 0) 
                                highlight_destination(0, 0, 1, 1); break;
                    }
                    switch(pos_index_in_horizontal_panel % 3)
                    {
                        case 0:
                            if(value[1, pos_index_in_horizontal_panel + 1, 0] == 0) 
                                highlight_destination(2, 1, pos_index_in_horizontal_panel + 1, 0); break;
                        case 1:
                            if (value[1, pos_index_in_horizontal_panel - 1, 0] == 0) 
                                highlight_destination(2, 1, pos_index_in_horizontal_panel - 1, 0);
                            if (value[1, pos_index_in_horizontal_panel + 1, 0] == 0) 
                                highlight_destination(3, 1, pos_index_in_horizontal_panel + 1, 1); break;
                        case 2:
                            if (value[1, pos_index_in_horizontal_panel - 1, 0] == 0) 
                                highlight_destination(2, 1, pos_index_in_horizontal_panel - 1, 1); break;
                    }
                    if (!one_or_more_path_highlighted)
                        Sm.play_sound(dies_is_blocked_sound);
                    break;
                case 1:
                    for(int i = 0; i < 24; i++)
                    {
                        if (value[0, i, 0] == 0)
                            positions[i / 3, i % 3].Stroke = highlight_color;
                    }
                    break;
            }
        }
        void highlight_destination(int hp_index, int target_panel, int pos_index, int path_index)
        {
            one_or_more_path_highlighted = true;
            Sm.play_sound(highlight_path_sound);
            switch(target_panel)
            {
                case 0:
                    position_index = pos_index_in_vertical_panel / 3;
                    highlighted_positions[hp_index] = positions[position_index, pos_index];
                    break;
                case 1:
                    position_index = pos_index_in_horizontal_panel / 3;                    
                    highlighted_positions[hp_index] = positions[get_X(pos_index), get_Y(pos_index)];
                    break;
            }
            highlighted_paths[hp_index] = path[target_panel, position_index, path_index];
            highlighted_paths[hp_index].Fill = highlighted_positions[hp_index].Stroke = highlight_color;
        }
        void make_normal_highlighted_destination(int mode)
        {
            switch(mode)
            {
                case 0:
                    for (int i = 0; i < 4; i++)
                        highlighted_paths[i].Fill = highlighted_positions[i].Stroke = normal_color;
                    break;
                case 1:
                    for (int i = 0; i < 8; i++)
                        for (int j = 0; j < 3; j++)
                            if (value[0, 3 * i + j, 0] == 0) positions[i, j].Stroke = normal_color;
                    recent_clicked_position.Stroke = normal_color;
                    break;
            }
        }

        int get_X(int index)
        {
            switch(index)
            {
                case 0:  case 9:  case 21: return 0;
                case 3:  case 10: case 18: return 1;
                case 6:  case 11: case 15: return 2;
                case 1:  case 4:  case 7:  return 3;
                case 16: case 19: case 22: return 4;
                case 8:  case 12: case 17: return 5;
                case 5:  case 13: case 20: return 6;
                case 2:  case 14: case 23: return 7;                    
            }
            return 99;
        }
        int get_Y(int index)
        {
            switch(index)
            {
                case 0: case 3: case 6: case 1: case 16: case 8: case 5: case 2: return 0;
                case 9: case 10:case 11:case 4: case 19: case 12:case 13:case 14:return 1;
                case 21:case 18:case 15:case 7: case 22: case 17:case 20:case 23:return 2;
            }
            return 99;
        }
        int get_conresponding_value_in_horizontal_panel(int index)
        {
            switch(index)
            {
                case 0: return 0;                case 1: return 9;                 case 2: return 21;
                case 3: return 3;                case 4: return 10;                case 5: return 18;
                case 6: return 6;                case 7: return 11;                case 8: return 15;
                case 9: return 1;                case 10: return 4;                case 11: return 7;
                case 12: return 16;              case 13: return 19;               case 14: return 22;
                case 15: return 8;               case 16: return 12;               case 17: return 17;
                case 18: return 5;               case 19: return 13;               case 20: return 20;
                case 21: return 2;               case 22: return 14;               case 23: return 23;
            }
            return 0;
        }

        bool check_for_trayo_in(int target_panel)
        {
            switch (target_panel)
            {
                case 0: position_index = pos_index_in_vertical_panel; break;
                case 1: position_index = pos_index_in_horizontal_panel; break;
            }
            position_index /= 3;
            position_index *= 3;
            if (value[target_panel, position_index, 0] == value[target_panel, position_index + 1, 0] &&
                value[target_panel, position_index + 1, 0] == value[target_panel, position_index + 2, 0])
                return true;
            else
                return false;
        }

        void click_setting_manager(int situation)
        {
            switch (situation)
            {
                case 0: Enable_click_on_position = Enable_click_on_dies = false; break;
                case 1: Enable_click_on_position = true; Enable_click_on_dies = false; break;
                case 2: Enable_click_on_position = false; Enable_click_on_dies = true; break;
                case 3: Enable_click_on_position = Enable_click_on_dies = true; break;
                case 4:
                    if (all_dies_placed)
                        click_setting_manager(3);
                    else
                    {
                        if (dies_Sno == 9)
                        {
                            all_dies_placed = true;
                            set_message(3);
                            click_setting_manager(3);
                        }
                        if (!all_dies_placed) click_setting_manager(1);
                    }
                    break;
            }
        }
        void change_grid_color()
        {
            switch (player_bit)
            {
                case 1:
                    change_back_color.Stop();
                    change_back_color.AutoReverse = true;
                    change_back_color.Begin();
                    change_back_color.Seek(TimeSpan.FromMilliseconds(500));
                    break;
                case 2:
                    change_back_color.AutoReverse = false;
                    change_back_color.Begin();
                    break;
            }
        }
        void change_player_bit()
        {
            opponent_player_bit = player_bit;
            switch (player_bit)
            {
                case 1: player_bit = 2; break;
                case 2: player_bit = 1; break;
            }
        }

        void option_clicked(object sender, RoutedEventArgs e)
        {
            switch(int.Parse(((Button)sender).Tag.ToString()))
            {
                case 1:
                    //home button
                    this.Frame.Navigate(typeof(MainPage));
                    break;
                case 2:
                    //restart button
                    this.Frame.Navigate(typeof(Game), my_colors);
                    break;
                case 3:
                    //sound button
                    if (!sound_OFF)
                    {
                        sound_button.Content = "SOUND ON";
                        sound_OFF = true;
                    }
                    else
                    {
                        sound_button.Content = "SOUND OFF";
                        sound_OFF = false;
                    }
                    destination_selected.IsMuted =
                    white_dies_placed_sound.IsMuted =
                    black_dies_placed_sound.IsMuted =
                    dies_is_in_mill_sound.IsMuted =
                    highlight_path_sound.IsMuted =
                    dies_is_blocked_sound.IsMuted = sound_OFF;
                    break;
            }
        }

    }
}