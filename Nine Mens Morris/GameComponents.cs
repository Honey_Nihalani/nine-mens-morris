﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

namespace Nine_Mens_Morris
{
    class GameComponents
    {
        Grid myGrid;
        Ellipse ellipse;
        Rectangle rectangle;
        Color[] my_colors;
        SolidColorBrush highlight_color, normal_color, trayo_color, trayo_color_Error, position_color, p1_dies_color, p2_dies_color;
        SolidColorBrush[] back_surface_color;

        public GameComponents(Color[] my_colors, Grid myGrid)
        {
            this.my_colors = my_colors;
            this.myGrid = myGrid;
            set_colors();
        }

        void set_colors()
        {
            back_surface_color = new SolidColorBrush[3];
            back_surface_color[0] = new SolidColorBrush(my_colors[2]);
            back_surface_color[1] = new SolidColorBrush(my_colors[3]);
            back_surface_color[2] = new SolidColorBrush(my_colors[4]);
            normal_color = new SolidColorBrush(my_colors[5]);
            highlight_color = new SolidColorBrush(my_colors[6]);
            trayo_color = new SolidColorBrush(my_colors[7]);
            trayo_color_Error = new SolidColorBrush(my_colors[8]);
            position_color = new SolidColorBrush(my_colors[9]);
            p1_dies_color = new SolidColorBrush(my_colors[10]);
            p2_dies_color = new SolidColorBrush(my_colors[11]);
        }

        public Rectangle get_back_surface(int i)
        {
            rectangle = new Rectangle();
            rectangle.Height = 600 - 200 * i;
            rectangle.Width = 1000 - 200 * i;
            rectangle.Fill = back_surface_color[i];
            myGrid.Children.Add(rectangle);
            return rectangle;
        }

        public Rectangle get_path(int i, int j, int k)
        {
            rectangle = new Rectangle();
            rectangle.Fill = normal_color;
            switch (i)
            {
                case 0:
                    rectangle.Height = 5;
                    switch (j)
                    {
                        case 0: case 1: case 2:
                            rectangle.Width = 500 - 100 * j;
                            rectangle.Margin = new Thickness((-500 + 100 * j) + (1000 - 200 * j) * k, -600 + 200 * j, 0, 0);
                            break;
                        case 3: case 4:
                            rectangle.Width = 100;
                            rectangle.Margin = new Thickness((-900 + 1600 * (j - 3)) + 200 * k, 0, 0, 0);
                            break;
                        case 5: case 6: case 7:
                            rectangle.Width = 300 + 100 * (j - 5);
                            rectangle.Margin = new Thickness((-300 - 100 * (j - 5)) + (600 + 200 * (j - 5)) * k, 200 + 200 * (j - 5), 0, 0);
                            break;
                    }
                    break;
                case 1:
                    rectangle.Width = 5;
                    switch (j)
                    {
                        case 0: case 1: case 2:
                            rectangle.Height = 300 - 100 * j;
                            rectangle.Margin = new Thickness(-1000 + 200 * j, (-300 + 100 * j) + (600 - 200 * j) * k, 0, 0);
                            break;
                        case 3: case 4:
                            rectangle.Height = 100;
                            rectangle.Margin = new Thickness(0, (-500 + 800 * (j - 3)) + 200 * k, 0, 0);
                            break;
                        case 5: case 6: case 7:
                            rectangle.Height = 100 + 100 * (j - 5);
                            rectangle.Margin = new Thickness(600 + 200 * (j - 5), (-100 - 100 * (j - 5)) + (200 + 200 * (j - 5)) * k, 0, 0);
                            break;
                    }
                    break;
            }
            myGrid.Children.Add(rectangle);
            return rectangle;
        }

        public Rectangle get_trayo(int i, int j)
        {
            rectangle = new Rectangle();
            rectangle.Fill = trayo_color;
            rectangle.RadiusX = rectangle.RadiusY = 30;
            rectangle.Opacity = 0;
            switch (i)
            {
                case 0:
                    rectangle.Height = 60;
                    switch (j)
                    {
                        case 0: case 1: case 2:
                            rectangle.Width = 1060 - 200 * j;
                            rectangle.Margin = new Thickness(0, -600 + 200 * j, 0, 0);
                            break;
                        case 3: case 4:
                            rectangle.Width = 260;
                            rectangle.Margin = new Thickness(-800 + 1600 * (j - 3), 0, 0, 0);
                            break;
                        case 5: case 6: case 7:
                            rectangle.Width = 660 + 200 * (j - 5);
                            rectangle.Margin = new Thickness(0, 200 + 200 * (j - 5), 0, 0);
                            break;
                    }
                    break;
                case 1:
                    rectangle.Width = 60;
                    switch (j)
                    {
                        case 0: case 1: case 2:
                            rectangle.Height = 660 - 200 * j;
                            rectangle.Margin = new Thickness(-1000 + 200 * j, 0, 0, 0);
                            break;
                        case 3: case 4:
                            rectangle.Height = 260;
                            rectangle.Margin = new Thickness(0, -400 + 800 * (j - 3), 0, 0);
                            break;
                        case 5: case 6: case 7:
                            rectangle.Height = 260 + 200 * (j - 5);
                            rectangle.Margin = new Thickness(600 + 200 * (j - 5), 0, 0, 0);
                            break;
                    }
                    break;
            }
            myGrid.Children.Add(rectangle);
            return rectangle;
        }

        public Ellipse get_position(int i, int j)
        {
            ellipse = new Ellipse();
            ellipse.Height = 60;
            ellipse.Width = 60;
            ellipse.Fill = position_color;
            ellipse.Stroke = normal_color;
            ellipse.StrokeThickness = 5 * (Window.Current.Bounds.Width / 1366);
            ellipse.Tag = (j + 3 * i).ToString();
            switch (i)
            {
                case 0: case 1: case 2:
                    ellipse.Margin = new Thickness((-1000 + 200 * i) + ((1000 - 200 * i) * j), -600 + 200 * i, 0, 0); break;
                case 3: case 4:
                    ellipse.Margin = new Thickness((-1000 + 1600 * (i - 3)) + 200 * j, 0, 0, 0); break;
                case 5: case 6: case 7:
                    ellipse.Margin = new Thickness((-600 - 200 * (i - 5)) + (600 + 200 * (i - 5)) * j, 200 + 200 * (i - 5), 0, 0); break;
            }
            myGrid.Children.Add(ellipse);
            return ellipse;
        }

        public Ellipse get_dies(int i, int j)
        {
            ellipse = new Ellipse();
            ellipse.Height = ellipse.Width = 60;
            switch(j)
            {
                case 0:
                    ellipse.Fill = p1_dies_color;
                    ellipse.Margin = new Thickness(-1200, -600 + 150 * i, 0, 0);
                    break;
                case 1:
                    ellipse.Fill = p2_dies_color;
                    ellipse.Margin = new Thickness(1200, -600 + 150 * i, 0, 0);
                    break;
            }
            ellipse.RenderTransform = new CompositeTransform();
            ellipse.RenderTransformOrigin = new Point(0.5, 0.5);
            myGrid.Children.Add(ellipse);
            return ellipse;
        }

        public void process_shape(Shape shape)
        {
            Thickness margin = shape.Margin;
            shape.Width *= 0.707174;
            shape.Height *= 0.70703125;
            margin.Left *= 0.707174;
            margin.Top *= 0.70703125;
            shape.Margin = margin;
        }

    }
}