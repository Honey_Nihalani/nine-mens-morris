﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.System;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Nine_Mens_Morris
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        bool my_colors_set, data_readed_from_file, components_generated_of_Custom_UI, components_generated_of_Game_UI, components_generated_of_How_to_play, pointer_exit_from_color_board_without_press;
        bool[] click_on_color;
        byte[,] c;
        int color_index = 0, show_page = 0, current_page = 1, previous_page_before_current_page;
        string theme_data = "";
        Button reset_theme;
        Color[] my_colors;
        Ellipse[] white_dies, black_dies, page_indicator;
        Ellipse[,] positions;
        Image next_page, pre_page;
        Rectangle trayo, trayo_Error, selected_color_board;
        Rectangle[] back_surface, color_value, pressed_rgb, color_board;
        Rectangle[,] rgb_color;
        Rectangle[, ,] path;
        SolidColorBrush currentPageIndicate, otherPageIndicate;
        TextBlock Description;
        StorageFile file;
        DoubleAnimation[] da_Tx, da_Opacity_CUI;
        Storyboard show_customise_theme, show_buttons, show_how_to_play;
        Storyboard[] change_color, page_animation;
        GameComponents Gc;
        AnimationManager Am;
        AudioManager Sm;
        MediaElement button_click_sound;
        ComponentMaintainer cm;

        public MainPage()
        {
            this.InitializeComponent();

            cm = new ComponentMaintainer(50000);
            maintainComponent();

            my_colors = new Color[12];
            c = new byte[12, 3];
            getFile();
            Am = new AnimationManager();
            create_sound_components();
            create_show_buttons();
            show_buttons.Begin();
        }

        void maintainComponent()
        {
            cm.maintainButton(play_button);
            cm.maintainButton(customise_button);
            cm.maintainButton(how_to_play_button);
            cm.maintainGrid(Game_UI);
            cm.maintainShape(background_1);
            cm.maintainShape(background_2);
            cm.maintainImage(back_button);
            cm.maintainHyperlink(link_to_facebook);
            cm.maintainHyperlink(link_to_googleP);
            cm.maintainHyperlink(link_to_twitter);
            cm.maintainHyperlink(link_to_more);
        }

        void create_sound_components()
        {
            Sm = new AudioManager(myGrid);
            button_click_sound = Sm.create_sound(1);
        }

        async void getFile()
        {
            try
            {
                file = await ApplicationData.Current.LocalFolder.GetFileAsync("theme.txt");
            }
            catch (Exception e)
            {
                creatFile();
            }
        }
        async void creatFile()
        {
            await ApplicationData.Current.LocalFolder.CreateFileAsync("theme.txt");
            file = await ApplicationData.Current.LocalFolder.GetFileAsync("theme.txt");
            //my_default theme:
            theme_data = "255187102085000000068017000085034017119051017255102085000255255000170000255000000119034000255255255000000000";
            await FileIO.WriteTextAsync(file, theme_data);
        }
        async void write_theme_data()
        {
            theme_data = "";
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (c[i, j] < 10) theme_data += "00" + c[i, j].ToString();
                    else if (c[i, j] < 100) theme_data += "0" + c[i, j].ToString();
                    else theme_data += c[i, j].ToString();
                }
            }
            await FileIO.WriteTextAsync(file, theme_data);
        }
        async void read_theme_data()
        {
            theme_data = await FileIO.ReadTextAsync(file);
            data_readed_from_file = true;
        }

        void create_show_buttons()
        {
            show_buttons = new Storyboard();
            DoubleAnimation[] da_rotate = Am.get_da_Rotate(show_buttons);
            Storyboard.SetTarget(da_rotate[0], how_to_play_button);
            Storyboard.SetTarget(da_rotate[1], customise_button);
            Storyboard.SetTarget(da_rotate[2], play_button);
            show_buttons.Completed += show_buttons_Completed;
        }
        void show_buttons_Completed(object sender, object e)
        {
            if (!data_readed_from_file)
                //2.
                read_theme_data();
            if (show_buttons.AutoReverse)
            {
                switch(show_page)
                {
                    case 1:
                        if (components_generated_of_Custom_UI) 
                            set_visibility_of(1, Visibility.Visible);
                        else
                        {
                            //3.
                            set_colors_from_theme_data();
                            Game_UI.Visibility = back_button.Visibility = Visibility.Visible;
                            create_show_customise_theme();
                            Generat_Components_For_Game_UI();
                            Generat_Components_For_Customise_UI();
                            //initial selected color board
                            selected_color_board = color_board[0];
                            create_change_color();
                        }
                        if (current_page != 1)
                        {
                            reset_previous_page(current_page);
                            set_Game_UI(1); 
                        }
                        link_to_more.Visibility = Visibility.Visible;
                        positions[1, 0].Stroke = positions[3, 0].Stroke = positions[3, 2].Stroke =
                        positions[6, 0].Stroke = path[0, 3, 0].Fill = path[0, 3, 1].Fill =
                        path[1, 1, 0].Fill = path[1, 1, 1].Fill = new SolidColorBrush(my_colors[6]);
                        trayo.Opacity = trayo_Error.Opacity = 1;
                        reset_theme.Visibility = Visibility.Visible;
                        show_customise_theme.AutoReverse = false;
                        show_customise_theme.Begin();
                        break;
                    case 2:
                        if (components_generated_of_How_to_play) 
                            set_visibility_of(2, Visibility.Visible);
                        else
                        {
                            //3.
                            set_colors_from_theme_data();
                            Game_UI.Visibility = back_button.Visibility = Visibility.Visible;
                            Generat_Components_For_Game_UI();
                            Generat_Components_For_How_To_Play();
                            create_show_how_to_play();
                            create_page_animations();
                        }
                        link_to_more.Visibility = link_to_facebook.Visibility = link_to_googleP.Visibility = link_to_twitter.Visibility = Visibility.Visible;
                        positions[1, 0].Stroke = positions[3, 0].Stroke = positions[3, 2].Stroke =
                        positions[6, 0].Stroke = path[0, 3, 0].Fill = path[0, 3, 1].Fill =
                        path[1, 1, 0].Fill = path[1, 1, 1].Fill = new SolidColorBrush(my_colors[5]);
                        trayo.Opacity = trayo_Error.Opacity = 0;
                        set_current_page();
                        reset_theme.Visibility = Visibility.Collapsed;
                        show_how_to_play.AutoReverse = false;
                        show_how_to_play.Begin();
                        break;
                }
            }
            else
                show_page = 0;
        }
  
        void button_clicked(object sender, RoutedEventArgs e)
        {
            Sm.play_sound(button_click_sound);
            int button_ID = int.Parse(((Button)sender).Tag.ToString());
            switch(button_ID)
            {
                case 1: //PLAY button
                    if (!my_colors_set)
                        set_colors_from_theme_data();
                    this.Frame.Navigate(typeof(Game), my_colors);
                    break;
                case 2: //Customise Button
                case 3: //How to play button
                    show_page = button_ID - 1;
                    Game_UI.Margin = new Thickness(show_page * 100, 30, 0, 0);
                    Game_UI.Margin = cm.resetMargin(Game_UI.Margin);
                    show_buttons.Stop();
                    show_buttons.AutoReverse = true;
                    show_buttons.Begin();
                    show_buttons.Seek(TimeSpan.FromMilliseconds(600));
                    break;
                case 4: //Reset colors button
                    theme_data = "255187102085000000068017000085034017119051017255102085000255255000170000255000000119034000255255255000000000";
                    set_colors_from_theme_data();
                    for (color_index = 0; color_index < 12; color_index++) 
                        set_new_color();
                    set_new_color_board();
                    break;
            }
        }

        void Generat_Components_For_Customise_UI()
        {
            if (!components_generated_of_Custom_UI)
            {
                components_generated_of_Custom_UI = true;
                pressed_rgb = new Rectangle[3];
                click_on_color = new bool[3];
                for (int i = 0; i < 3; i++)
                    click_on_color[i] = true;

                rgb_color = new Rectangle[3, 16];
                color_value = new Rectangle[3];

                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 16; j++)
                    {
                        rgb_color[i, j] = new Rectangle();
                        rgb_color[i, j].Height = rgb_color[i, j].Width = 30;
                        rgb_color[i, j].Opacity = 0;
                        switch (i)
                        {
                            case 0:
                                rgb_color[i, j].Fill = new SolidColorBrush(Color.FromArgb(255, (byte)(0 + 17 * j), 0, 0));
                                rgb_color[i, j].PointerPressed += PointerPressed_on_Red;
                                break;
                            case 1:
                                rgb_color[i, j].Fill = new SolidColorBrush(Color.FromArgb(255, 0, (byte)(0 + 17 * j), 0));
                                rgb_color[i, j].PointerPressed += PointerPressed_on_Green;
                                break;
                            case 2:
                                rgb_color[i, j].Fill = new SolidColorBrush(Color.FromArgb(255, 0, 0, (byte)(0 + 17 * j)));
                                rgb_color[i, j].PointerPressed += PointerPressed_on_Blue;
                                break;
                        }
                        rgb_color[i, j].Tag = (17 * j).ToString();
                        rgb_color[i, j].Margin = new Thickness(-950 + 100 * j, 450 + 100 * i, 0, 0);
                        Storyboard.SetTarget(da_Opacity_CUI[i * 16 + j], rgb_color[i, j]);
                        cm.maintainShape(rgb_color[i, j]);
                        myGrid.Children.Add(rgb_color[i, j]);
                    }
                }
                for (int i = 0; i < 3; i++)
                {
                    color_value[i] = new Rectangle();
                    color_value[i].Width = color_value[i].Height = 44;
                    color_value[i].StrokeThickness = 5;
                    color_value[i].Stroke = new SolidColorBrush(Colors.White);
                    color_value[i].Margin = new Thickness(-1050, 450 + 100 * i, 0, 0);
                    color_value[i].RenderTransform = new CompositeTransform();
                    color_value[i].Opacity = 0;
                    Storyboard.SetTarget(da_Opacity_CUI[48 + i], color_value[i]);
                    cm.maintainShape(color_value[i]);
                    myGrid.Children.Add(color_value[i]);
                }
                color_board = new Rectangle[12];
                for (int i = 0; i < 12; i++)
                {
                    color_board[i] = new Rectangle();
                    color_board[i].Height = color_board[i].Width = 80;
                    color_board[i].Fill = new SolidColorBrush(my_colors[i]);
                    color_board[i].Margin = new Thickness(950 + 250 * (i % 2), -625 + 250 * (i / 2), 0, 0);
                    color_board[i].RenderTransform = new CompositeTransform();
                    color_board[i].RenderTransformOrigin = new Point(0.5, 0.5);
                    color_board[i].Opacity = 0;
                    Storyboard.SetTarget(da_Opacity_CUI[51 + i], color_board[i]);
                    color_board[i].Tag = i.ToString();
                    color_board[i].PointerPressed += PointerPressed_on_color_board;
                    color_board[i].PointerEntered += PointerEntered_in_color_board;
                    color_board[i].PointerExited += PointerExited_from_color_board;
                    cm.maintainShape(color_board[i]);
                    myGrid.Children.Add(color_board[i]);
                }
            }
        }
        void Generat_Components_For_How_To_Play()
        {
            if (!components_generated_of_How_to_play)
            {
                components_generated_of_How_to_play = true;

                currentPageIndicate = new SolidColorBrush(Colors.White);
                otherPageIndicate = new SolidColorBrush(Color.FromArgb(255, 110, 110, 110));

                Description = new TextBlock();
                Description.Width = 1200;
                Description.FontSize = 35;
                Description.TextAlignment = TextAlignment.Center;
                Description.TextWrapping = TextWrapping.Wrap;
                Description.FontFamily = new FontFamily("Tempus Sans ITC");
                Description.Opacity = 0;
                Description.Margin = new Thickness(0, 550, 0, 0);
                cm.maintainTextblock(Description);
                myGrid.Children.Add(Description);

                pre_page = new Image();
                next_page = new Image();
                pre_page.Width = next_page.Width = 100;
                pre_page.Height = next_page.Height = 200;
                pre_page.Source = new BitmapImage(new Uri(@"ms-appx:///images/left_arrow.png", UriKind.RelativeOrAbsolute));
                next_page.Source = new BitmapImage(new Uri(@"ms-appx:///images/right_arrow.png", UriKind.RelativeOrAbsolute));
                pre_page.Tag = 1;
                next_page.Tag = 2;
                pre_page.Stretch = next_page.Stretch = Stretch.Fill;
                pre_page.Margin = new Thickness(-1200, 0, 0, 0);
                next_page.Margin = new Thickness(1200, 0, 0, 0);
                pre_page.PointerEntered += PointerEntered_in_image_button;
                pre_page.PointerExited += PointerExited_from_image_button;
                pre_page.PointerPressed += PointerPressed_on_image_button;
                next_page.PointerEntered += PointerEntered_in_image_button;
                next_page.PointerExited += PointerExited_from_image_button;
                next_page.PointerPressed += PointerPressed_on_image_button;
                cm.maintainImage(pre_page);
                cm.maintainImage(next_page);
                myGrid.Children.Add(pre_page);
                myGrid.Children.Add(next_page);

                page_indicator = new Ellipse[10];
                for (int i = 0; i < 10; i++)
                {
                    page_indicator[i] = new Ellipse();
                    page_indicator[i].Width = 10;
                    page_indicator[i].Height = 10;
                    page_indicator[i].Fill = otherPageIndicate;
                    page_indicator[i].Margin = new Thickness(-400 + 80 * i, 720, 0, 0);
                    cm.maintainShape(page_indicator[i]);
                    myGrid.Children.Add(page_indicator[i]);
                }
            }
        }
        void Generat_Components_For_Game_UI()
        {
            if (!components_generated_of_Game_UI)
            {
                components_generated_of_Game_UI = true;
                Gc = new GameComponents(my_colors, Game_UI);
                current_player_color_1.Color = my_colors[0];
                opponent_player_color_1.Color =
                opponent_player_color_2.Color = 
                opponent_player_color_3.Color = my_colors[1];

                back_surface = new Rectangle[3];
                for (int i = 0; i < 3; i++)
                {
                    back_surface[i] = Gc.get_back_surface(i);
                    Gc.process_shape(back_surface[i]);
                    cm.maintainShape(back_surface[i]);
                }

                path = new Rectangle[2, 8, 2];
                for (int i = 0; i < 2; i++)
                {
                    for (int j = 0; j < 8; j++)
                    {
                        for (int k = 0; k < 2; k++)
                        {
                            path[i, j, k] = Gc.get_path(i, j, k);
                            Gc.process_shape(path[i, j, k]);
                            cm.maintainShape(path[i, j, k]);
                        }
                    }
                }

                trayo = Gc.get_trayo(0, 2);
                Gc.process_shape(trayo);
                cm.maintainShape(trayo);
                trayo.RadiusX = trayo.RadiusY = trayo.Height / 2;

                trayo_Error = Gc.get_trayo(1, 4);
                Gc.process_shape(trayo_Error);
                cm.maintainShape(trayo_Error);
                trayo_Error.Fill = new SolidColorBrush(my_colors[8]);
                trayo_Error.RadiusX = trayo_Error.RadiusY = trayo_Error.Width / 2;

                positions = new Ellipse[8, 3];
                for (int i = 0; i < 8; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        positions[i, j] = Gc.get_position(i, j);
                        Gc.process_shape(positions[i, j]);
                        cm.maintainShape(positions[i, j]);
                        positions[i, j].StrokeThickness *= 0.707;
                    }
                }

                white_dies = new Ellipse[9];
                black_dies = new Ellipse[9];
                for (int i = 0; i < 9; i++)
                {
                    white_dies[i] = Gc.get_dies(i, 0);
                    black_dies[i] = Gc.get_dies(i, 1);
                    Gc.process_shape(white_dies[i]);
                    Gc.process_shape(black_dies[i]);
                    cm.maintainShape(white_dies[i]);
                    cm.maintainShape(black_dies[i]);
                }
                reset_theme = new Button();
                reset_theme.Height = 50;
                reset_theme.Width = 300;              
                reset_theme.FontSize = 25;
                reset_theme.Margin = new Thickness(333, 0, 0, 0);
                reset_theme.FontFamily = new FontFamily("Tempus Sans ITC");
                reset_theme.BorderThickness = new Thickness(0, 0, 0, 0); //reset_theme.BorderThickness.Right = reset_theme.BorderThickness.Top = reset_theme.BorderThickness.Bottom = 0;
                reset_theme.Foreground = new SolidColorBrush(Colors.White);
                reset_theme.Content = "SeT DefaulT ColorS";
                reset_theme.Tag = 4;
                reset_theme.Click += button_clicked;
                cm.maintainButton(reset_theme);
                Game_UI.Children.Add(reset_theme);
            }
        }

        void create_show_how_to_play()
        {
            show_how_to_play = new Storyboard();
            DoubleAnimation[] da_Opacity = new DoubleAnimation[14];
            for (int i = 0; i < 14; i++)
            {
                da_Opacity[i] = Am.get_da_show(show_how_to_play);
                if (i > 3) Storyboard.SetTarget(da_Opacity[i], page_indicator[i - 4]);
            }
            Storyboard.SetTarget(da_Opacity[0], Game_UI);
            Storyboard.SetTarget(da_Opacity[1], Description);
            Storyboard.SetTarget(da_Opacity[2], next_page);
            Storyboard.SetTarget(da_Opacity[3], pre_page);
            show_how_to_play.Completed += show_how_to_play_Completed;
        }
        void show_how_to_play_Completed(object sender, object e)
        {
            if (show_how_to_play.AutoReverse)
                set_visibility_of(2, Visibility.Collapsed);
        }
        void create_show_customise_theme()
        {
            show_customise_theme = new Storyboard();
            da_Opacity_CUI = new DoubleAnimation[64];
            for (int i = 0; i < 64; i++)
                da_Opacity_CUI[i] = Am.get_da_show(show_customise_theme);
            Storyboard.SetTarget(da_Opacity_CUI[63], Game_UI);
            show_customise_theme.Completed += show_customise_theme_Completed;
        }
        void show_customise_theme_Completed(object sender, object e)
        {
            if (show_customise_theme.AutoReverse)
                set_visibility_of(1, Visibility.Collapsed);
            else
                set_new_color_board();
        }
        void create_page_animations()
        {
            DoubleAnimation da_;
            page_animation = new Storyboard[11];
            for (int i = 0; i < 11; i++)
            {
                page_animation[i] = new Storyboard();
                if (i == 0 || i == 3 || i == 7) continue;
                page_animation[i].Completed += page_animation_completed;
            }

            // animation of page 2:
            for (int i = 0; i < 7; i++)
            {
                white_dies[i].RenderTransformOrigin = new Point(0.5, 0.5);
                da_ = Am.get_da_Sx(page_animation[0], 1);
                da_.AutoReverse = true;
                da_.RepeatBehavior = RepeatBehavior.Forever;
                Storyboard.SetTarget(da_, white_dies[i]);
                if(i < 4)
                {
                    black_dies[i].RenderTransformOrigin = new Point(0.5, 0.5);
                    da_ = Am.get_da_Sy(page_animation[0], 2);
                    Storyboard.SetTarget(da_, black_dies[i]);
                }
            }

            // animation of page 3:
            da_ = Am.get_da_Tx(page_animation[1], 1000);
            da_.To = (positions[0, 1].Margin.Left - white_dies[4].Margin.Left) / 2;
            Storyboard.SetTarget(da_, white_dies[4]);
            da_ = Am.get_da_Ty(page_animation[1], 700);
            da_.To = (positions[0, 1].Margin.Top - white_dies[4].Margin.Top) / 2;
            Storyboard.SetTarget(da_, white_dies[4]);
            da_ = Am.get_da_Tx(page_animation[2], 1000);
            da_.To = (positions[1, 1].Margin.Left - black_dies[4].Margin.Left) / 2;
            Storyboard.SetTarget(da_, black_dies[4]);
            da_ = Am.get_da_Ty(page_animation[2], 700);
            da_.To = (positions[1, 1].Margin.Top - black_dies[4].Margin.Top) / 2;
            Storyboard.SetTarget(da_, black_dies[4]);

            // animation of page 4
            da_ = Am.get_da_Tx(page_animation[3], 1000);
            da_.To = (positions[2, 2].Margin.Left - white_dies[5].Margin.Left) / 2;
            Storyboard.SetTarget(da_, white_dies[5]);
            da_ = Am.get_da_Ty(page_animation[3], 700);
            da_.To = (positions[2, 2].Margin.Top - white_dies[5].Margin.Top) / 2;
            Storyboard.SetTarget(da_, white_dies[5]);
            da_ = Am.get_da_show(page_animation[3]);
            da_.BeginTime = TimeSpan.FromSeconds(1);
            Storyboard.SetTarget(da_, trayo);
            da_ = Am.get_da_hide(page_animation[3]);
            da_.BeginTime = TimeSpan.FromSeconds(2);
            Storyboard.SetTarget(da_, black_dies[4]);

            // animation of page 5
            da_ = Am.get_da_Ty(page_animation[4], 700);
            da_.To = path[1, 2, 0].Height;
            Storyboard.SetTarget(da_, white_dies[0]);
            da_ = Am.get_da_Tx(page_animation[5], 700);
            da_.To = path[0, 6, 0].Width;
            Storyboard.SetTarget(da_, black_dies[6]);

            // animation of page 6
            da_ = Am.get_da_Ty(page_animation[6], 700);
            da_.To = -path[1, 2, 0].Height;
            Storyboard.SetTarget(da_, white_dies[0]);
            da_ = Am.get_da_show(page_animation[7]);
            Storyboard.SetTarget(da_, trayo);
            da_ = Am.get_da_hide(page_animation[7]);
            da_.BeginTime = TimeSpan.FromMilliseconds(500);
            Storyboard.SetTarget(da_, black_dies[7]);

            //animation of page 8
            da_ = Am.get_da_Tx(page_animation[8], 1000);
            da_.To =  path[0, 7, 0].Width;
            Storyboard.SetTarget(da_, black_dies[2]);
            da_ = Am.get_da_Ty(page_animation[8], 700);
            da_.To = -path[1, 7, 0].Height - path[1, 6, 0].Height;
            Storyboard.SetTarget(da_, black_dies[2]);

            //animation of page 9
            da_ = Am.get_da_Ty(page_animation[9], 700);
            da_.To = -path[1, 2, 0].Height;
            Storyboard.SetTarget(da_, white_dies[0]);
            da_ = Am.get_da_show(page_animation[10]);
            Storyboard.SetTarget(da_, trayo);
            da_ = Am.get_da_hide(page_animation[10]);
            da_.BeginTime = TimeSpan.FromMilliseconds(500);
            Storyboard.SetTarget(da_, black_dies[4]);
        }
        void page_animation_completed(object sender, object e)
        {
            if (sender.Equals(page_animation[1]))
            {
                page_animation[1].Stop();
                white_dies[4].Margin = positions[0, 1].Margin;
                current_player_color_1.Color = my_colors[1];
                opponent_player_color_3.Color = my_colors[0];
                page_animation[2].Begin();
            }
            else if (sender.Equals(page_animation[2]))
            {
                page_animation[2].Stop();
                black_dies[4].Margin = positions[1, 1].Margin;
                current_player_color_1.Color = my_colors[0];
                opponent_player_color_3.Color = my_colors[1];
            }
            else if (sender.Equals(page_animation[4]))
            {
                page_animation[4].Stop();
                current_player_color_1.Color = my_colors[1];
                opponent_player_color_3.Color = my_colors[0];
                white_dies[0].Margin = positions[3, 2].Margin;
                path[1, 2, 0].Fill = positions[3, 2].Stroke = new SolidColorBrush(my_colors[5]);
                path[0, 6, 0].Fill = positions[6, 2].Stroke = new SolidColorBrush(my_colors[6]);
                page_animation[5].Begin();
            }
            else if (sender.Equals(page_animation[5]))
            {
                page_animation[5].Stop();
                black_dies[6].Margin = positions[6, 1].Margin;
                path[0, 6, 0].Fill = positions[6, 2].Stroke = new SolidColorBrush(my_colors[5]);
                current_player_color_1.Color = my_colors[0];
                opponent_player_color_3.Color = my_colors[1];
            }
            else if (sender.Equals(page_animation[6]))
            {
                page_animation[6].Stop();
                white_dies[0].Margin = positions[2, 0].Margin;
                path[1, 2, 0].Fill = positions[2, 0].Stroke = new SolidColorBrush(my_colors[5]);
                page_animation[7].Begin();
            }
            else if (sender.Equals(page_animation[8]))
            {
                page_animation[8].Stop();
                black_dies[2].Margin = positions[1, 1].Margin;
                positions[0, 0].Stroke = positions[1, 0].Stroke = positions[1, 1].Stroke =
                positions[2, 0].Stroke = positions[3, 1].Stroke = positions[4, 0].Stroke =
                positions[4, 2].Stroke = positions[5, 0].Stroke = positions[5, 1].Stroke =
                positions[6, 0].Stroke = positions[6, 1].Stroke = positions[6, 2].Stroke =
                positions[7, 1].Stroke = positions[7, 2].Stroke = new SolidColorBrush(my_colors[5]);
                current_player_color_1.Color = my_colors[0];
                opponent_player_color_3.Color = my_colors[1];
            }
            else if (sender.Equals(page_animation[9]))
            {
                page_animation[9].Stop();
                white_dies[0].Margin = positions[2, 0].Margin;
                path[1, 2, 0].Fill = positions[2, 0].Stroke = new SolidColorBrush(my_colors[5]);
                page_animation[10].Begin();
            }
            else
            {
                page_animation[10].Stop();
                black_dies[4].Opacity = 0;
                current_player_color_1.Color = opponent_player_color_3.Color = my_colors[10];
                page_animation[0].Begin();
            }
        }
        void create_change_color()
        {
            change_color = new Storyboard[3];
            da_Tx = new DoubleAnimation[3];
            for(int i = 0; i < 3; i++)
            {
                change_color[i] = new Storyboard();
                da_Tx[i] = Am.get_da_Tx(change_color[i], 200);
                Storyboard.SetTarget(da_Tx[i], color_value[i]);
                change_color[i].Completed += change_color_Completed; 
            }
        }
        void change_color_Completed(object sender, object e)
        {
            int id = 0;
            if (sender.Equals(change_color[0])) id = 0;
            else if (sender.Equals(change_color[1])) id = 1;
            else id = 2;
            change_color[id].Stop();
            c[color_index, id] = byte.Parse(pressed_rgb[id].Tag.ToString());
            color_value[id].Margin = pressed_rgb[id].Margin;
            click_on_color[id] = true;
            set_new_color();
        }

        void set_visibility_of(int target, Visibility visibility)
        {
            switch(target)
            {
                case 1:
                    back_button.Visibility =
                    Game_UI.Visibility = visibility;
                    for (int i = 0; i < 3; i++)
                    {
                        color_value[i].Visibility = visibility;
                        for (int j = 0; j < 16; j++)
                            rgb_color[i, j].Visibility = visibility;
                    }
                    for (int i = 0; i < 12; i++)
                        color_board[i].Visibility = visibility;
                    break;
                case 2:
                    for (int i = 0; i < 10; i++)
                        page_indicator[i].Visibility = visibility;
                    Game_UI.Visibility =
                    back_button.Visibility =
                    Description.Visibility =
                    next_page.Visibility =
                    pre_page.Visibility = visibility;
                    break;
            }
        }
        void set_new_color()
        {
            my_colors[color_index] = Color.FromArgb(255, c[color_index, 0], c[color_index, 1], c[color_index, 2]);
            color_board[color_index].Fill = new SolidColorBrush(my_colors[color_index]);
            switch(color_index)
            {
                case 0:
                    current_player_color_1.Color = my_colors[color_index];
                    break;
                case 1:
                    opponent_player_color_1.Color =
                    opponent_player_color_2.Color = 
                    opponent_player_color_3.Color = my_colors[color_index];
                    break;
                case 2:
                    back_surface[0].Fill = new SolidColorBrush(my_colors[color_index]);
                    break;
                case 3:
                    back_surface[1].Fill = new SolidColorBrush(my_colors[color_index]);
                    break;
                case 4:
                    back_surface[2].Fill = new SolidColorBrush(my_colors[color_index]);
                    break;
                case 5:
                    for (int i = 0; i < 2; i++)
                        for (int j = 0; j < 8; j++)
                            for (int k = 0; k < 2; k++)
                                path[i, j, k].Fill = new SolidColorBrush(my_colors[color_index]);
                    for (int i = 0; i < 8; i++)
                        for (int j = 0; j < 3; j++)
                            positions[i, j].Stroke = new SolidColorBrush(my_colors[color_index]);
                    positions[1, 0].Stroke = positions[3, 0].Stroke =
                    positions[3, 2].Stroke = positions[6, 0].Stroke =
                    path[0, 3, 0].Fill = path[0, 3, 1].Fill =
                    path[1, 1, 0].Fill = path[1, 1, 1].Fill = new SolidColorBrush(my_colors[6]);
                    break;
                case 6:
                    positions[1, 0].Stroke = positions[3, 0].Stroke =
                    positions[3, 2].Stroke = positions[6, 0].Stroke =
                    path[0, 3, 0].Fill = path[0, 3, 1].Fill =
                    path[1, 1, 0].Fill = path[1, 1, 1].Fill = new SolidColorBrush(my_colors[color_index]);
                    break;
                case 7:
                    trayo.Fill = new SolidColorBrush(my_colors[color_index]);
                    break;
                case 8:
                    trayo_Error.Fill = new SolidColorBrush(my_colors[color_index]);
                    break;
                case 9:
                    for (int i = 0; i < 8; i++)
                        for (int j = 0; j < 3; j++)
                            positions[i, j].Fill = new SolidColorBrush(my_colors[color_index]);
                    break;
                case 10:
                    for (int i = 0; i < 9; i++)
                        white_dies[i].Fill = new SolidColorBrush(my_colors[color_index]);
                    break;
                case 11:
                    for (int i = 0; i < 9; i++)
                        black_dies[i].Fill = new SolidColorBrush(my_colors[color_index]);
                    break;
            }
        }
        void set_new_color_board()
        {
            currently_selected_color_board.Stop();
            color_index = int.Parse(selected_color_board.Tag.ToString());
            for (int i = 0; i < 3; i++)
                set_color_value(i, rgb_color[i, c[color_index, i] / 17]);
            selected_color_board.Width = selected_color_board.Height = 100 * cm.widthFector;
            Storyboard.SetTarget(da_swing, selected_color_board);
            currently_selected_color_board.Begin();
        }
        void set_color_value(int rgb, Rectangle sender)
        {
            click_on_color[rgb] = false;
            pressed_rgb[rgb] = sender;
            da_Tx[rgb].To = (pressed_rgb[rgb].Margin.Left - color_value[rgb].Margin.Left) / 2;
            change_color[rgb].Begin();
        }
        void set_colors_from_theme_data()
        {
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 3; j++)
                    c[i, j] = byte.Parse(theme_data.Substring(i * 9 + j * 3, 3));
                my_colors[i] = Color.FromArgb(255, c[i, 0], c[i, 1], c[i, 2]);
            }
            my_colors_set = true;
        }
        void set_current_page()
        {
            reset_previous_page(previous_page_before_current_page);
            set_Game_UI(current_page);
            set_Description_Text();
            set_page_indicator();
        }
        void reset_previous_page(int page)
        {
            switch (page)
            {
                case 1:
                    break;
                case 2:
                    page_animation[0].Stop();
                    break;
                case 3:
                    if (page_animation[1].GetCurrentState() == ClockState.Active)
                        page_animation[1].Stop();
                    else if (page_animation[2].GetCurrentState() == ClockState.Active)
                        page_animation[2].Stop();
                    break;
                case 4:
                    page_animation[3].Stop();
                    break;
                case 5:
                    if (page_animation[4].GetCurrentState() == ClockState.Active)
                    {
                        page_animation[4].Stop();
                        path[1, 2, 0].Fill = positions[3, 2].Stroke = new SolidColorBrush(my_colors[5]);
                    }
                    else if (page_animation[5].GetCurrentState() == ClockState.Active)
                    {
                        page_animation[5].Stop();
                        path[0, 6, 0].Fill = positions[6, 2].Stroke = new SolidColorBrush(my_colors[5]);
                    }
                    break;
                case 6:
                    if (page_animation[6].GetCurrentState() == ClockState.Active)
                    {
                        page_animation[6].Stop();
                        path[1, 2, 0].Fill = positions[2, 0].Stroke = new SolidColorBrush(my_colors[5]);
                    }
                    else if (page_animation[7].GetCurrentState() == ClockState.Active)
                        page_animation[7].Stop();
                    break;
                case 7:
                    positions[0, 0].Stroke = positions[1, 0].Stroke = positions[1, 1].Stroke =
                    positions[2, 0].Stroke = positions[3, 1].Stroke = positions[4, 0].Stroke =
                    positions[4, 2].Stroke = positions[5, 0].Stroke = positions[5, 1].Stroke =
                    positions[6, 0].Stroke = positions[6, 1].Stroke = positions[6, 2].Stroke =
                    positions[7, 1].Stroke = positions[7, 2].Stroke = new SolidColorBrush(my_colors[5]);
                    break;
                case 8:
                    if (page_animation[8].GetCurrentState() == ClockState.Active)
                    {
                        page_animation[8].Stop();
                        positions[0, 0].Stroke = positions[1, 0].Stroke = positions[1, 1].Stroke =
                        positions[2, 0].Stroke = positions[3, 1].Stroke = positions[4, 0].Stroke =
                        positions[4, 2].Stroke = positions[5, 0].Stroke = positions[5, 1].Stroke =
                        positions[6, 0].Stroke = positions[6, 1].Stroke = positions[6, 2].Stroke =
                        positions[7, 1].Stroke = positions[7, 2].Stroke = new SolidColorBrush(my_colors[5]);
                    }
                    break;
                case 9:
                    if (page_animation[9].GetCurrentState() == ClockState.Active)
                    {
                        page_animation[9].Stop();
                        path[1, 2, 0].Fill = positions[2, 0].Stroke = new SolidColorBrush(my_colors[5]);
                    }
                    else if (page_animation[10].GetCurrentState() == ClockState.Active)
                        page_animation[10].Stop();
                    else if (page_animation[0].GetCurrentState() == ClockState.Active)
                        page_animation[0].Stop();
                    break;
                case 10:
                    page_animation[0].Stop();
                    break;
            }
            for (int i = 0; i < 9; i++)
                white_dies[i].Opacity = black_dies[i].Opacity = 1;
            current_player_color_1.Color = my_colors[0];
            opponent_player_color_3.Color = my_colors[1];
            trayo.Opacity = trayo_Error.Opacity = 0;
        }
        void set_Game_UI(int page)
        {
            switch (page)
            {
                case 1:
                    for (int i = 0; i < 9; i++)
                    {
                        white_dies[i].Margin = new Thickness(-1200 * 0.7071, (-600 + 150 * i) * 0.707, 0, 0);
                        black_dies[i].Margin = new Thickness(1200 * 0.7071, (-600 + 150 * i) * 0.707, 0, 0);
                        white_dies[i].Margin = cm.resetMargin(white_dies[i].Margin);
                        black_dies[i].Margin = cm.resetMargin(black_dies[i].Margin);
                    }
                    break;
                case 2:
                    white_dies[0].Margin = positions[6, 0].Margin;
                    white_dies[1].Margin = positions[1, 0].Margin;
                    white_dies[2].Margin = positions[2, 0].Margin;
                    white_dies[3].Margin = positions[2, 2].Margin;
                    white_dies[4].Margin = positions[3, 2].Margin;
                    white_dies[5].Margin = positions[5, 0].Margin;
                    white_dies[6].Margin = positions[5, 2].Margin;
                    white_dies[7].Margin = new Thickness(-1200 * 0.7071, 450 * 0.707, 0, 0);
                    white_dies[7].Margin = cm.resetMargin(white_dies[7].Margin);
                    white_dies[8].Margin = new Thickness(-1200 * 0.7071, 600 * 0.707, 0, 0);
                    white_dies[8].Margin = cm.resetMargin(white_dies[8].Margin);
                    white_dies[7].Opacity = 0;
                    white_dies[8].Opacity = 0;
                    black_dies[0].Margin = positions[1, 1].Margin;
                    black_dies[1].Margin = positions[4, 1].Margin;
                    for (int i = 0; i < 9; i++)
                    {
                        if (i < 2) black_dies[i].Opacity = 1;
                        else black_dies[i].Opacity = 0;
                    }
                    current_player_color_1.Color = opponent_player_color_3.Color = my_colors[10];
                    page_animation[0].Begin();
                    break;
                case 3:
                case 4:
                    for (int i = 4; i < 9; i++)
                    {
                        white_dies[i].Margin = new Thickness(-1200 * 0.7071, (-600 + 150 * i) * 0.707, 0, 0);
                        white_dies[i].Margin = cm.resetMargin(white_dies[i].Margin);
                        black_dies[i].Margin = new Thickness(1200 * 0.7071, (-600 + 150 * i) * 0.707, 0, 0);
                        black_dies[i].Margin = cm.resetMargin(black_dies[i].Margin);
                    }
                    white_dies[0].Margin = positions[2, 0].Margin;
                    white_dies[1].Margin = positions[2, 1].Margin;
                    white_dies[2].Margin = positions[3, 0].Margin;
                    white_dies[3].Margin = positions[5, 2].Margin;
                    black_dies[0].Margin = positions[0, 2].Margin;
                    black_dies[1].Margin = positions[4, 1].Margin;
                    black_dies[2].Margin = positions[7, 0].Margin;
                    black_dies[3].Margin = positions[1, 0].Margin;
                    switch (current_page)
                    {
                        case 3:
                            page_animation[1].Begin();
                            break;
                        case 4:
                            white_dies[4].Margin = positions[0, 1].Margin;
                            black_dies[4].Margin = positions[1, 1].Margin;
                            page_animation[3].Begin();
                            break;
                    }
                    break;
                case 5:
                case 6:
                    white_dies[1].Margin = positions[2, 1].Margin;
                    white_dies[2].Margin = positions[3, 0].Margin;
                    white_dies[3].Margin = positions[5, 2].Margin;
                    white_dies[4].Margin = positions[0, 1].Margin;
                    white_dies[5].Margin = positions[2, 2].Margin;
                    white_dies[6].Margin = positions[1, 2].Margin;
                    white_dies[7].Margin = positions[7, 1].Margin;
                    white_dies[8].Opacity = 0;
                    black_dies[0].Margin = positions[0, 2].Margin;
                    black_dies[1].Margin = positions[4, 1].Margin;
                    black_dies[2].Margin = positions[7, 0].Margin;
                    black_dies[3].Margin = positions[1, 0].Margin;
                    black_dies[4].Margin = positions[1, 1].Margin;
                    black_dies[7].Margin = positions[3, 1].Margin;
                    black_dies[8].Margin = positions[6, 2].Margin;
                    black_dies[5].Opacity = 0;
                    switch (current_page)
                    {
                        case 5:
                            white_dies[0].Margin = positions[2, 0].Margin;
                            black_dies[6].Margin = positions[6, 0].Margin;
                            path[1, 2, 0].Fill = positions[3, 2].Stroke = new SolidColorBrush(my_colors[6]);
                            page_animation[4].Begin();
                            break;
                        case 6:
                            white_dies[0].Margin = positions[3, 2].Margin;
                            black_dies[6].Margin = positions[6, 1].Margin;
                            path[1, 2, 0].Fill = positions[2, 0].Stroke = new SolidColorBrush(my_colors[6]);
                            page_animation[6].Begin();
                            break;
                    }
                    break;
                case 7:
                case 8:
                    current_player_color_1.Color = my_colors[1];
                    opponent_player_color_3.Color = my_colors[0];
                    white_dies[0].Margin = positions[3, 2].Margin;
                    white_dies[1].Margin = positions[2, 1].Margin;
                    white_dies[2].Margin = positions[3, 0].Margin;
                    white_dies[3].Margin = positions[5, 2].Margin;
                    white_dies[4].Margin = positions[0, 1].Margin;
                    white_dies[5].Margin = positions[2, 2].Margin;
                    white_dies[6].Margin = positions[1, 2].Margin;
                    white_dies[7].Opacity =
                    white_dies[8].Opacity = 0;
                    black_dies[0].Margin = positions[0, 2].Margin;
                    black_dies[1].Margin = positions[4, 1].Margin;
                    black_dies[2].Margin = positions[7, 0].Margin;
                    for (int i = 3; i < 9; i++)
                        black_dies[i].Opacity = 0;
                    positions[0, 0].Stroke = positions[1, 0].Stroke = positions[1, 1].Stroke =
                    positions[2, 0].Stroke = positions[3, 1].Stroke = positions[4, 0].Stroke =
                    positions[4, 2].Stroke = positions[5, 0].Stroke = positions[5, 1].Stroke =
                    positions[6, 0].Stroke = positions[6, 1].Stroke = positions[6, 2].Stroke =
                    positions[7, 1].Stroke = positions[7, 2].Stroke = new SolidColorBrush(my_colors[6]);
                    if (current_page == 8)
                        page_animation[8].Begin();
                    break;
                case 9:
                    white_dies[0].Margin = positions[3, 2].Margin;
                    white_dies[1].Margin = positions[2, 1].Margin;
                    white_dies[2].Margin = positions[3, 0].Margin;
                    white_dies[3].Margin = positions[5, 2].Margin;
                    white_dies[4].Margin = positions[0, 1].Margin;
                    white_dies[5].Margin = positions[2, 2].Margin;
                    white_dies[6].Margin = positions[1, 2].Margin;
                    white_dies[7].Opacity = 0;
                    white_dies[8].Opacity = 0;
                    black_dies[0].Margin = positions[0, 2].Margin;
                    black_dies[1].Margin = positions[4, 1].Margin;
                    black_dies[4].Margin = positions[1, 1].Margin;
                    for (int i = 2; i < 9; i++)
                    {
                        if (i == 4) continue;
                        black_dies[i].Opacity = 0;
                    }
                    path[1, 2, 0].Fill = positions[2, 0].Stroke = new SolidColorBrush(my_colors[6]);
                    page_animation[9].Begin();
                    break;
                case 10:
                    current_player_color_1.Color =
                    opponent_player_color_3.Color = my_colors[10];
                    white_dies[0].Margin = positions[1, 1].Margin;
                    white_dies[1].Margin = positions[4, 1].Margin;
                    white_dies[2].Margin = positions[6, 1].Margin;
                    white_dies[3].Margin = positions[3, 1].Margin;
                    white_dies[4].Margin = positions[5, 0].Margin;
                    white_dies[5].Margin = positions[5, 1].Margin;
                    white_dies[6].Margin = positions[3, 2].Margin;
                    white_dies[7].Opacity = 0;
                    white_dies[8].Opacity = 0;
                    black_dies[0].Margin = positions[1, 0].Margin;
                    black_dies[1].Margin = positions[1, 2].Margin;
                    black_dies[2].Margin = positions[6, 2].Margin;
                    black_dies[3].Margin = positions[6, 0].Margin;
                    black_dies[4].Opacity = 0;
                    black_dies[5].Opacity = 0;
                    black_dies[6].Opacity = 0;
                    black_dies[7].Opacity = 0;
                    black_dies[8].Opacity = 0;
                    page_animation[0].Begin();
                    break;
            }
        }
        void set_Description_Text()
        {
            switch (current_page)
            {
                case 1:
                    Description.Height = 150;
                    pre_page.Visibility = Visibility.Collapsed;
                    Description.Text = "The board consists of a grid with 24 vacent positions. Each player has 9 PIECEs (MENs), Players try to form MILLs (i.e, 3 of their own pieces lined) allowing a player to remove an opponent's men from the game";
                    break;
                case 2:
                    Description.Height = 150;
                    pre_page.Visibility = Visibility.Visible;
                    Description.Text = "A player WINS by reducing the opponent to two pieces (where he could no longer form MILLS and thus be unable to win), or by leaving him without a legal move";
                    break;
                case 3:
                    Description.Height = 100;
                    Description.Text = "The game proceeds in 3 PHASES:" + '\n' + "PHASE 1: placing men on vacant points turn by turn";
                    break;
                case 4:
                    Description.Height = 150;
                    Description.Text = "if player formed a mill, then player can remove one of his opponent's pieces from the board. Any piece can be chosen for the removal, but a piece not in an opponent's mill must be selected, if possible";
                    break;
                case 5:
                    Description.Height = 50;
                    Description.Text = "PHASE 2: moving men to adjacent points";
                    break;
                case 6:
                    Description.Height = 150;
                    Description.Text = "In second phase A player may break a mill by moving one of his pieces out of an existing mill, then moving the piece back to form the same mill a second time (or any number of times), each time removing one of his opponent's men";
                    break;
                case 7:
                    Description.Height = 100;
                    Description.Text = "PHASE 3: (optional phase) moving men to any vacant point when a player has been reduced to three men";
                    break;
                case 8:
                    Description.Height = 150;
                    Description.Text = "When a player is reduced to three pieces, there is no longer a limitation of moving to only adjacent points (i.e., The player's men may FLY from any point to any vacant point.)";
                    break;
                case 9:
                    Description.Height = 100;
                    next_page.Visibility = Visibility.Visible;
                    Description.Text = "A player WINS by reducing the opponent to two pieces (i.e., where he could no longer form MILLS)";
                    break;
                case 10:
                    Description.Height = 100;
                    next_page.Visibility = Visibility.Collapsed;
                    Description.Text = "A player also can WINS by leaving opponent in such a condition in which he can't able to make legal move";
                    break;
            }
        }
        void set_page_indicator()
        {
            int i = current_page;
            page_indicator[i - 1].Fill = currentPageIndicate;
            page_indicator[i - 1].Width = page_indicator[i - 1].Height = 15 * cm.widthFector;
            if (i == 1)
            {
                page_indicator[i].Fill = otherPageIndicate;
                page_indicator[i].Width = page_indicator[i].Height = 10 * cm.widthFector;
            }
            else if (i == 10)
            {
                page_indicator[i - 2].Fill = otherPageIndicate;
                page_indicator[i - 2].Width = page_indicator[i - 2].Height = 10 * cm.widthFector;
            }
            else
            {
                page_indicator[i].Fill = page_indicator[i - 2].Fill = otherPageIndicate;
                page_indicator[i].Width = page_indicator[i].Height = 
                page_indicator[i - 2].Width = page_indicator[i - 2].Height = 10 * cm.widthFector;
            }
        }

        void PointerPressed_on_Red(object sender, PointerRoutedEventArgs e)
        {
            if (click_on_color[0])
                set_color_value(0, (Rectangle)sender);
        }
        void PointerPressed_on_Green(object sender, PointerRoutedEventArgs e)
        {
            if (click_on_color[1])
                set_color_value(1, (Rectangle)sender);
        }
        void PointerPressed_on_Blue(object sender, PointerRoutedEventArgs e)
        {
            if (click_on_color[2])
                set_color_value(2, (Rectangle)sender);
        }

        void PointerEntered_in_color_board(object sender, PointerRoutedEventArgs e)
        {
            if (!sender.Equals(selected_color_board))
            {
                pointer_exit_from_color_board_without_press = true;
                Rectangle rect = (Rectangle)sender;
                rect.Height = rect.Width = 90 * cm.widthFector;
            }
        }
        void PointerExited_from_color_board(object sender, PointerRoutedEventArgs e)
        {
            if(pointer_exit_from_color_board_without_press)
            {
                if (!sender.Equals(selected_color_board))
                {
                    Rectangle rect = (Rectangle)sender;
                    rect.Height = rect.Width = 80 * cm.widthFector;
                }
            }
       }
        void PointerPressed_on_color_board(object sender, PointerRoutedEventArgs e)
        {
            pointer_exit_from_color_board_without_press = false;            
            selected_color_board.Width = selected_color_board.Height = 80 * cm.widthFector;
            selected_color_board = (Rectangle)sender;
            set_new_color_board();
        }

        void PointerEntered_in_image_button(object sender, PointerRoutedEventArgs e)
        {
            ((Image)sender).Opacity = 0.8;
        }
        void PointerExited_from_image_button(object sender, PointerRoutedEventArgs e)
        {
            ((Image)sender).Opacity = 1;
        }
        void PointerPressed_on_image_button(object sender, PointerRoutedEventArgs e)
        {
            int button_ID = int.Parse(((Image)sender).Tag.ToString());
            switch (button_ID)
            {
                case 0:
                    back_button.Visibility = Visibility.Collapsed;
                    show_buttons.Stop();
                    show_buttons.AutoReverse = false;
                    show_buttons.Begin();
                    switch (show_page)
                    {
                        case 1:
                            write_theme_data();
                            link_to_more.Visibility = Visibility.Collapsed;
                            show_customise_theme.Stop();
                            show_customise_theme.AutoReverse = true;
                            show_customise_theme.Begin();
                            show_customise_theme.Seek(TimeSpan.FromMilliseconds(500));
                            break;
                        case 2:
                            link_to_more.Visibility = link_to_facebook.Visibility = link_to_googleP.Visibility = link_to_twitter.Visibility = Visibility.Collapsed;
                            show_how_to_play.Stop();
                            show_how_to_play.AutoReverse = true;
                            show_how_to_play.Begin();
                            show_how_to_play.Seek(TimeSpan.FromMilliseconds(500));
                            break;
                    }
                    break;
                case 1:
                    // previous image
                    if (current_page > 1)
                    {
                        previous_page_before_current_page = current_page;
                        current_page--;
                    }
                    break;
                case 2:
                    // next image
                    if (current_page < 10)
                    {
                        previous_page_before_current_page = current_page;
                        current_page++;
                    }
                    break;
            }
            if (button_ID != 0)
                set_current_page();
        }

        void PointerEntered_in_linkButton(object sender, PointerRoutedEventArgs e)
        {
            ((HyperlinkButton)sender).Opacity = 1;
        }
        void PointerExited_from_linkButton(object sender, PointerRoutedEventArgs e)
        {
            ((HyperlinkButton)sender).Opacity = 0.7;
        }

        void myGrid_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (show_page == 2)
            {
                switch (e.Key)
                {
                    case VirtualKey.Right:
                        if (current_page < 10)
                        {
                            previous_page_before_current_page = current_page;
                            current_page++;
                            set_current_page();
                        }
                        break;
                    case VirtualKey.Left:
                        if (current_page > 1)
                        {
                            previous_page_before_current_page = current_page;
                            current_page--;
                            set_current_page();
                        }
                        break;
                    case VirtualKey.Back:
                        link_to_more.Visibility = link_to_facebook.Visibility = link_to_googleP.Visibility = link_to_twitter.Visibility = Visibility.Collapsed;
                        back_button.Visibility = Visibility.Collapsed;
                        show_buttons.Stop();
                        show_buttons.AutoReverse = false;
                        show_buttons.Begin();
                        show_how_to_play.Stop();
                        show_how_to_play.AutoReverse = true;
                        show_how_to_play.Begin();
                        show_how_to_play.Seek(TimeSpan.FromMilliseconds(500));
                        break;
                }
            }
            if(show_page == 1 && e.Key == VirtualKey.Back)
            {
                back_button.Visibility = Visibility.Collapsed;
                show_buttons.Stop();
                show_buttons.AutoReverse = false;
                show_buttons.Begin();
                write_theme_data();
                link_to_more.Visibility = Visibility.Collapsed; 
                show_customise_theme.Stop();
                show_customise_theme.AutoReverse = true;
                show_customise_theme.Begin();
                show_customise_theme.Seek(TimeSpan.FromMilliseconds(500));
            }
        }

    }
}