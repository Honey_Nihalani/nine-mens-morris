﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Shapes;

namespace Nine_Mens_Morris
{
    class ComponentMaintainer
    {
        Thickness margin;
        public double widthFector, heightFector, fontDiff, windowWidth, windowHeight;

        public ComponentMaintainer(double font)
        {
            windowWidth = Window.Current.Bounds.Width;
            windowHeight = Window.Current.Bounds.Height;

            if (ApplicationView.Value == ApplicationViewState.FullScreenPortrait)
            {
                if (windowHeight / windowWidth < 1.3)
                {
                    heightFector = windowHeight / 768;
                    widthFector = (windowHeight * 1.77864583333) / 1366;
                    fontDiff = (((windowHeight * windowHeight * 1.77) - (1366 * 768)) / font);
                }
                else
                {
                    heightFector = windowWidth / 768;
                    widthFector = windowHeight / 1366;
                    fontDiff = (((windowWidth * windowHeight) - (1366 * 768)) / font);
                }
            }
            else
            {
                widthFector = windowWidth / 1366;
                heightFector = windowHeight / 768;
                fontDiff = (((windowWidth * windowHeight) - (1366 * 768)) / font);
            }
        }

        public void maintainAppbar(AppBar appbar)
        {
            appbar.Width *= widthFector;
            appbar.Height *= heightFector;
        }

        public void maintainButton(Button button)
        {
            button.Width *= widthFector;
            button.Height *= heightFector;
            button.FontSize += (fontDiff / 2);
            margin = button.Margin;
            setMargin();
            button.Margin = margin;
        }

        public void maintainGrid(Grid grid)
        {
            grid.Width *= widthFector;
            grid.Height *= heightFector;
            margin = grid.Margin;
            setMargin();
            grid.Margin = margin;
        }

        public void maintainHyperlink(HyperlinkButton hlButton)
        {
            hlButton.Width *= widthFector;
            hlButton.Height *= heightFector;
            margin = hlButton.Margin;
            setMargin();
            hlButton.Margin = margin;
        }

        public void maintainImage(Image image)
        {
            image.Width *= widthFector;
            image.Height *= heightFector;
            margin = image.Margin;
            setMargin();
            image.Margin = margin;
        }

        public void maintainShape(Shape shape)
        {
            shape.Width *= widthFector;
            shape.Height *= heightFector;
            margin = shape.Margin;
            setMargin();
            shape.Margin = margin;
        }

        public void maintainTextblock(TextBlock textblock)
        {
            textblock.Width *= widthFector;
            textblock.Height *= heightFector;
            textblock.FontSize += (fontDiff / 3.5);
            margin = textblock.Margin;
            setMargin();
            textblock.Margin = margin;
        }

        private void setMargin()
        {
            margin.Top *= heightFector;
            margin.Left *= widthFector;
            margin.Bottom *= heightFector;
            margin.Right *= widthFector;
        }

        public Thickness resetMargin(Thickness margin)
        {
            margin.Top *= heightFector;
            margin.Left *= widthFector;
            margin.Bottom *= heightFector;
            margin.Right *= widthFector;
            return margin;
        }

    }
}
