﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Nine_Mens_Morris
{
    class AudioManager
    {
        private Grid myGrid;

        public bool mute_effect_sound;

        MediaElement sound;

        public AudioManager(Grid myGrid)
        {
            this.myGrid = myGrid;
        }

        public MediaElement create_sound(int id)
        {
            sound = new MediaElement();
            sound.Source = new Uri(@"ms-appx:///Sounds/" + id + ".wav", UriKind.RelativeOrAbsolute);
            sound.AutoPlay = false;
            sound.IsMuted = mute_effect_sound;
            myGrid.Children.Add(sound);
            return sound;
        }

        public void play_sound(MediaElement sound)
        {
            sound.Position = TimeSpan.FromSeconds(0);
            sound.Play();
        }

    }
}
