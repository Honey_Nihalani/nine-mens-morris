﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Animation;

namespace Nine_Mens_Morris
{
    class AnimationManager
    {
        DoubleAnimation da;
        DoubleAnimation[] da_;

        public DoubleAnimation[] get_da_Rotate(Storyboard animation)
        {
            da_ = new DoubleAnimation[3];
            for (int i = 0; i < 3; i++)
            {
                da_[i] = new DoubleAnimation();
                da_[i].Duration = new Duration(TimeSpan.FromMilliseconds(200));
                if (i == 1) da_[i].From = 90;
                else da_[i].From = -90;
                da_[i].To = 0;
                Storyboard.SetTargetProperty(da_[i], "(UIElement.RenderTransform).(CompositeTransform.Rotation)");
                animation.Children.Add(da_[i]);
            }
            return da_;
        }

        public DoubleAnimation[] get_da_Txy(Storyboard animation)
        {
            da_ = new DoubleAnimation[2];
            da_[0] = get_da_Tx(animation, 300);
            da_[1] = get_da_Ty(animation, 150);
            return da_;
        }

        public DoubleAnimation get_da_Tx(Storyboard animation, int time)
        {
            da = new DoubleAnimation();
            process_Transform(animation, time, 0);
            return da;
        }

        public DoubleAnimation get_da_Ty(Storyboard animation, int time)
        {
            da = new DoubleAnimation();
            process_Transform(animation, time, 1);
            return da;
        }

        void process_Transform(Storyboard animation, int time, int mode)
        {
            da.Duration = new Duration(TimeSpan.FromMilliseconds(time));
            switch (mode)
            {
                case 0: Storyboard.SetTargetProperty(da, "(UIElement.RenderTransform).(CompositeTransform.TranslateX)"); break;
                case 1: Storyboard.SetTargetProperty(da, "(UIElement.RenderTransform).(CompositeTransform.TranslateY)"); break;
            }
            animation.Children.Add(da);
        }

        public DoubleAnimation get_da_Sx(Storyboard animation, int time)
        {
            da = new DoubleAnimation();
            da.Duration = new Duration(TimeSpan.FromSeconds(time));
            da.From = 1;
            da.To = 0;
            Storyboard.SetTargetProperty(da, "(UIElement.RenderTransform).(CompositeTransform.ScaleX)");
            animation.Children.Add(da);
            return da;
        }

        public DoubleAnimation get_da_Sy(Storyboard animation, int time)
        {
            da = new DoubleAnimation();
            da.Duration = new Duration(TimeSpan.FromSeconds(time));
            da.From = 1;
            da.To = 0.1;
            Storyboard.SetTargetProperty(da, "(UIElement.RenderTransform).(CompositeTransform.ScaleY)");
            animation.Children.Add(da);
            return da;
        }

        public DoubleAnimation get_da_hide(Storyboard animation)
        {
            da = new DoubleAnimation();
            process_Opacity(animation, 0);
            return da;
        }

        public DoubleAnimation get_da_show(Storyboard animation)
        {
            da = new DoubleAnimation();
            process_Opacity(animation, 1);
            return da;
        }

        void process_Opacity(Storyboard animation, int mode)
        {
            da.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            switch (mode)
            {
                case 0: da.From = 1; da.To = 0; break;
                case 1: da.From = 0; da.To = 1; break;
            }
            Storyboard.SetTargetProperty(da, "(UIElement.Opacity)");
            animation.Children.Add(da);
        }

    }
}
